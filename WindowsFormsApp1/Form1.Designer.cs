﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bTrazi = new System.Windows.Forms.Button();
            this.tbRijec = new System.Windows.Forms.TextBox();
            this.lBPojmovi = new System.Windows.Forms.ListBox();
            this.lbRezultat = new System.Windows.Forms.Label();
            this.lbPojam = new System.Windows.Forms.Label();
            this.lbSlovo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bTrazi
            // 
            this.bTrazi.Location = new System.Drawing.Point(208, 25);
            this.bTrazi.Name = "bTrazi";
            this.bTrazi.Size = new System.Drawing.Size(84, 35);
            this.bTrazi.TabIndex = 0;
            this.bTrazi.Text = "Trazi";
            this.bTrazi.UseVisualStyleBackColor = true;
            this.bTrazi.Click += new System.EventHandler(this.bTrazi_Click);
            // 
            // tbRijec
            // 
            this.tbRijec.Location = new System.Drawing.Point(75, 33);
            this.tbRijec.Name = "tbRijec";
            this.tbRijec.Size = new System.Drawing.Size(98, 20);
            this.tbRijec.TabIndex = 1;
            // 
            // lBPojmovi
            // 
            this.lBPojmovi.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.lBPojmovi.FormattingEnabled = true;
            this.lBPojmovi.Location = new System.Drawing.Point(36, 122);
            this.lBPojmovi.Name = "lBPojmovi";
            this.lBPojmovi.Size = new System.Drawing.Size(165, 95);
            this.lBPojmovi.TabIndex = 2;
            // 
            // lbRezultat
            // 
            this.lbRezultat.AutoSize = true;
            this.lbRezultat.Location = new System.Drawing.Point(259, 187);
            this.lbRezultat.Name = "lbRezultat";
            this.lbRezultat.Size = new System.Drawing.Size(52, 13);
            this.lbRezultat.TabIndex = 3;
            this.lbRezultat.Text = "Rezultat: ";
            // 
            // lbPojam
            // 
            this.lbPojam.AutoSize = true;
            this.lbPojam.Location = new System.Drawing.Point(259, 138);
            this.lbPojam.Name = "lbPojam";
            this.lbPojam.Size = new System.Drawing.Size(42, 13);
            this.lbPojam.TabIndex = 4;
            this.lbPojam.Text = "Pojam: ";
            // 
            // lbSlovo
            // 
            this.lbSlovo.AutoSize = true;
            this.lbSlovo.Location = new System.Drawing.Point(33, 36);
            this.lbSlovo.Name = "lbSlovo";
            this.lbSlovo.Size = new System.Drawing.Size(37, 13);
            this.lbSlovo.TabIndex = 5;
            this.lbSlovo.Text = "Slovo:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 258);
            this.Controls.Add(this.lbSlovo);
            this.Controls.Add(this.lbPojam);
            this.Controls.Add(this.lbRezultat);
            this.Controls.Add(this.lBPojmovi);
            this.Controls.Add(this.tbRijec);
            this.Controls.Add(this.bTrazi);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bTrazi;
        private System.Windows.Forms.TextBox tbRijec;
        private System.Windows.Forms.ListBox lBPojmovi;
        private System.Windows.Forms.Label lbRezultat;
        private System.Windows.Forms.Label lbPojam;
        private System.Windows.Forms.Label lbSlovo;
    }
}

