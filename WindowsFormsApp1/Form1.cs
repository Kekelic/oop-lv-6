﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        List<string> lista = new List<string>();
        private string pojam;
        private int found = 0, zivoti = 5, numFound=0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load_1(object sender, EventArgs e)
        {

            using (System.IO.StreamReader reader = new System.IO.StreamReader("Pojmovi.txt"))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    lista.Add(line);
                }
            }
            lBPojmovi.DataSource = null;
            lBPojmovi.DataSource = lista;
            pojam = lista[0];
            lbRezultat.Text = zivoti.ToString();
        }

   

        private void bTrazi_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < pojam.Length; i++)
            {
                if (pojam[i].ToString() == tbRijec.Text)
                {
                    lbPojam.Text += pojam[i];
                    found = 1;
                    numFound++;
                }
            }

            if (found != 1)
            {
                zivoti--;
                lbRezultat.Text = zivoti.ToString();
            }

            found = 0;

            if (zivoti == 0)
            {
                MessageBox.Show("Game over");
                Application.Exit();
            }
            if (numFound == pojam.Length)
            {
                MessageBox.Show("Bravoo!!");
                Application.Exit();
            }
        }

    }
}
