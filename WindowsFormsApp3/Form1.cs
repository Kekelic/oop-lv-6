﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private double a, b;

        private void bZbroji_Click(object sender, EventArgs e)
        {
            double.TryParse(tbOperand1.Text, out a);
            double.TryParse(tbOperand2.Text, out b);
            lbRezultat.Text = "Rezultat: "+(a + b).ToString();
        }

        private void bOduzmi_Click(object sender, EventArgs e)
        {
            double.TryParse(tbOperand1.Text, out a);
            double.TryParse(tbOperand2.Text, out b);
            lbRezultat.Text = "Rezultat: " + (a - b).ToString();
        }

        private void bPomnozi_Click(object sender, EventArgs e)
        {
            double.TryParse(tbOperand1.Text, out a);
            double.TryParse(tbOperand2.Text, out b);
            lbRezultat.Text = "Rezultat: " + (a * b).ToString();
        }

        private void bPodijeli_Click(object sender, EventArgs e)
        {
            double.TryParse(tbOperand1.Text, out a);
            double.TryParse(tbOperand2.Text, out b);
            if (b == 0)
            {
                MessageBox.Show("Pogreska pri dijeljenju s 0!", "Pogreska");
            }
            lbRezultat.Text = "Rezultat: " + (a / b).ToString();
        }

        private void bSin_Click(object sender, EventArgs e)
        {
            double.TryParse(tbOperand.Text, out a);
            lbZnRezultat.Text = "Rezultat: " + Math.Sin(a*(3.14/180)).ToString();
        }

        private void bCos_Click(object sender, EventArgs e)
        {
            double.TryParse(tbOperand.Text, out a);
            lbZnRezultat.Text = "Rezultat: " + Math.Cos(a*(3.14 / 180)).ToString();
        }

        private void bPow_Click(object sender, EventArgs e)
        {
            double.TryParse(tbOperand.Text, out a);
            lbZnRezultat.Text = "Rezultat: " + Math.Pow(a,2.0).ToString();
        }

        private void bSqrt_Click(object sender, EventArgs e)
        {
            double.TryParse(tbOperand.Text, out a);
            lbZnRezultat.Text = "Rezultat: " + Math.Sqrt(a).ToString();
        }

        private void bExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void bLog_Click(object sender, EventArgs e)
        {
            double.TryParse(tbOperand.Text, out a);
            lbZnRezultat.Text = "Rezultat: " + Math.Log10(a).ToString();
        }
    }
}
