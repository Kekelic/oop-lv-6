﻿namespace WindowsFormsApp3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbOperand1 = new System.Windows.Forms.Label();
            this.lbOperand2 = new System.Windows.Forms.Label();
            this.tbOperand1 = new System.Windows.Forms.TextBox();
            this.tbOperand2 = new System.Windows.Forms.TextBox();
            this.bZbroji = new System.Windows.Forms.Button();
            this.bOduzmi = new System.Windows.Forms.Button();
            this.bPomnozi = new System.Windows.Forms.Button();
            this.bPodijeli = new System.Windows.Forms.Button();
            this.bSin = new System.Windows.Forms.Button();
            this.bCos = new System.Windows.Forms.Button();
            this.bLog = new System.Windows.Forms.Button();
            this.bSqrt = new System.Windows.Forms.Button();
            this.bPow = new System.Windows.Forms.Button();
            this.lbRezultat = new System.Windows.Forms.Label();
            this.lbOperand = new System.Windows.Forms.Label();
            this.tbOperand = new System.Windows.Forms.TextBox();
            this.lbZnRezultat = new System.Windows.Forms.Label();
            this.bExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbOperand1
            // 
            this.lbOperand1.AutoSize = true;
            this.lbOperand1.Location = new System.Drawing.Point(37, 28);
            this.lbOperand1.Name = "lbOperand1";
            this.lbOperand1.Size = new System.Drawing.Size(52, 13);
            this.lbOperand1.TabIndex = 0;
            this.lbOperand1.Text = "operand1";
            // 
            // lbOperand2
            // 
            this.lbOperand2.AutoSize = true;
            this.lbOperand2.Location = new System.Drawing.Point(37, 55);
            this.lbOperand2.Name = "lbOperand2";
            this.lbOperand2.Size = new System.Drawing.Size(52, 13);
            this.lbOperand2.TabIndex = 1;
            this.lbOperand2.Text = "operand2";
            // 
            // tbOperand1
            // 
            this.tbOperand1.Location = new System.Drawing.Point(95, 25);
            this.tbOperand1.Name = "tbOperand1";
            this.tbOperand1.Size = new System.Drawing.Size(100, 20);
            this.tbOperand1.TabIndex = 2;
            // 
            // tbOperand2
            // 
            this.tbOperand2.Location = new System.Drawing.Point(95, 52);
            this.tbOperand2.Name = "tbOperand2";
            this.tbOperand2.Size = new System.Drawing.Size(100, 20);
            this.tbOperand2.TabIndex = 3;
            // 
            // bZbroji
            // 
            this.bZbroji.Location = new System.Drawing.Point(272, 19);
            this.bZbroji.Name = "bZbroji";
            this.bZbroji.Size = new System.Drawing.Size(47, 30);
            this.bZbroji.TabIndex = 4;
            this.bZbroji.Text = "+";
            this.bZbroji.UseVisualStyleBackColor = true;
            this.bZbroji.Click += new System.EventHandler(this.bZbroji_Click);
            // 
            // bOduzmi
            // 
            this.bOduzmi.Location = new System.Drawing.Point(325, 19);
            this.bOduzmi.Name = "bOduzmi";
            this.bOduzmi.Size = new System.Drawing.Size(46, 30);
            this.bOduzmi.TabIndex = 5;
            this.bOduzmi.Text = "-";
            this.bOduzmi.UseVisualStyleBackColor = true;
            this.bOduzmi.Click += new System.EventHandler(this.bOduzmi_Click);
            // 
            // bPomnozi
            // 
            this.bPomnozi.Location = new System.Drawing.Point(271, 55);
            this.bPomnozi.Name = "bPomnozi";
            this.bPomnozi.Size = new System.Drawing.Size(47, 31);
            this.bPomnozi.TabIndex = 6;
            this.bPomnozi.Text = "*";
            this.bPomnozi.UseVisualStyleBackColor = true;
            this.bPomnozi.Click += new System.EventHandler(this.bPomnozi_Click);
            // 
            // bPodijeli
            // 
            this.bPodijeli.Location = new System.Drawing.Point(324, 55);
            this.bPodijeli.Name = "bPodijeli";
            this.bPodijeli.Size = new System.Drawing.Size(47, 29);
            this.bPodijeli.TabIndex = 8;
            this.bPodijeli.Text = "/";
            this.bPodijeli.UseVisualStyleBackColor = true;
            this.bPodijeli.Click += new System.EventHandler(this.bPodijeli_Click);
            // 
            // bSin
            // 
            this.bSin.Location = new System.Drawing.Point(219, 180);
            this.bSin.Name = "bSin";
            this.bSin.Size = new System.Drawing.Size(46, 29);
            this.bSin.TabIndex = 9;
            this.bSin.Text = "sin";
            this.bSin.UseVisualStyleBackColor = true;
            this.bSin.Click += new System.EventHandler(this.bSin_Click);
            // 
            // bCos
            // 
            this.bCos.Location = new System.Drawing.Point(272, 180);
            this.bCos.Name = "bCos";
            this.bCos.Size = new System.Drawing.Size(47, 29);
            this.bCos.TabIndex = 10;
            this.bCos.Text = "cos";
            this.bCos.UseVisualStyleBackColor = true;
            this.bCos.Click += new System.EventHandler(this.bCos_Click);
            // 
            // bLog
            // 
            this.bLog.Location = new System.Drawing.Point(325, 180);
            this.bLog.Name = "bLog";
            this.bLog.Size = new System.Drawing.Size(47, 28);
            this.bLog.TabIndex = 11;
            this.bLog.Text = "log";
            this.bLog.UseVisualStyleBackColor = true;
            this.bLog.Click += new System.EventHandler(this.bLog_Click);
            // 
            // bSqrt
            // 
            this.bSqrt.Location = new System.Drawing.Point(219, 229);
            this.bSqrt.Name = "bSqrt";
            this.bSqrt.Size = new System.Drawing.Size(46, 28);
            this.bSqrt.TabIndex = 12;
            this.bSqrt.Text = "sqrt";
            this.bSqrt.UseVisualStyleBackColor = true;
            this.bSqrt.Click += new System.EventHandler(this.bSqrt_Click);
            // 
            // bPow
            // 
            this.bPow.Location = new System.Drawing.Point(272, 229);
            this.bPow.Name = "bPow";
            this.bPow.Size = new System.Drawing.Size(47, 28);
            this.bPow.TabIndex = 13;
            this.bPow.Text = "pow";
            this.bPow.UseVisualStyleBackColor = true;
            this.bPow.Click += new System.EventHandler(this.bPow_Click);
            // 
            // lbRezultat
            // 
            this.lbRezultat.AutoSize = true;
            this.lbRezultat.Location = new System.Drawing.Point(79, 88);
            this.lbRezultat.Name = "lbRezultat";
            this.lbRezultat.Size = new System.Drawing.Size(49, 13);
            this.lbRezultat.TabIndex = 14;
            this.lbRezultat.Text = "Rezultat:";
            // 
            // lbOperand
            // 
            this.lbOperand.AutoSize = true;
            this.lbOperand.Location = new System.Drawing.Point(37, 205);
            this.lbOperand.Name = "lbOperand";
            this.lbOperand.Size = new System.Drawing.Size(48, 13);
            this.lbOperand.TabIndex = 15;
            this.lbOperand.Text = "Operand";
            // 
            // tbOperand
            // 
            this.tbOperand.Location = new System.Drawing.Point(95, 202);
            this.tbOperand.Name = "tbOperand";
            this.tbOperand.Size = new System.Drawing.Size(100, 20);
            this.tbOperand.TabIndex = 16;
            // 
            // lbZnRezultat
            // 
            this.lbZnRezultat.AutoSize = true;
            this.lbZnRezultat.Location = new System.Drawing.Point(82, 253);
            this.lbZnRezultat.Name = "lbZnRezultat";
            this.lbZnRezultat.Size = new System.Drawing.Size(49, 13);
            this.lbZnRezultat.TabIndex = 17;
            this.lbZnRezultat.Text = "Rezultat:";
            // 
            // bExit
            // 
            this.bExit.Location = new System.Drawing.Point(343, 253);
            this.bExit.Name = "bExit";
            this.bExit.Size = new System.Drawing.Size(56, 38);
            this.bExit.TabIndex = 18;
            this.bExit.Text = "Exit";
            this.bExit.UseVisualStyleBackColor = true;
            this.bExit.Click += new System.EventHandler(this.bExit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(401, 294);
            this.Controls.Add(this.bExit);
            this.Controls.Add(this.lbZnRezultat);
            this.Controls.Add(this.tbOperand);
            this.Controls.Add(this.lbOperand);
            this.Controls.Add(this.lbRezultat);
            this.Controls.Add(this.bPow);
            this.Controls.Add(this.bSqrt);
            this.Controls.Add(this.bLog);
            this.Controls.Add(this.bCos);
            this.Controls.Add(this.bSin);
            this.Controls.Add(this.bPodijeli);
            this.Controls.Add(this.bPomnozi);
            this.Controls.Add(this.bOduzmi);
            this.Controls.Add(this.bZbroji);
            this.Controls.Add(this.tbOperand2);
            this.Controls.Add(this.tbOperand1);
            this.Controls.Add(this.lbOperand2);
            this.Controls.Add(this.lbOperand1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbOperand1;
        private System.Windows.Forms.Label lbOperand2;
        private System.Windows.Forms.TextBox tbOperand1;
        private System.Windows.Forms.TextBox tbOperand2;
        private System.Windows.Forms.Button bZbroji;
        private System.Windows.Forms.Button bOduzmi;
        private System.Windows.Forms.Button bPomnozi;
        private System.Windows.Forms.Button bPodijeli;
        private System.Windows.Forms.Button bSin;
        private System.Windows.Forms.Button bCos;
        private System.Windows.Forms.Button bLog;
        private System.Windows.Forms.Button bSqrt;
        private System.Windows.Forms.Button bPow;
        private System.Windows.Forms.Label lbRezultat;
        private System.Windows.Forms.Label lbOperand;
        private System.Windows.Forms.TextBox tbOperand;
        private System.Windows.Forms.Label lbZnRezultat;
        private System.Windows.Forms.Button bExit;
    }
}

